﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeSpawner : MonoBehaviour
{
    [SerializeField] private GameObject cellPrefab;
    private GameObject m_maze;
    private MazeGenerator generator;
    private MazeGeneratorCell[,] maze;

    public MazeGeneratorCell[,] Maze
    {
        get { return maze; }
    }


    private void Start()
    {
        GameController.Instance.OnPushButtonPutPlayerOnField += DisableTriggerCollidersOnCells;
    }

    private void DisableTriggerCollidersOnCells()
    {
        if (generator != null)
        {
            // Делаем крайние ячейки не интерактивными, чтобы нельзя было на них ткнуть.
            for (int x = 0; x < maze.GetLength(0); x++)
            {
                for (int y = 0; y < maze.GetLength(1); y++)
                {
                    if (x != maze.GetLength(0) - 1 || y != maze.GetLength(1) - 1)
                    {
                        maze[x, y].Cell.Collider.enabled = false;
                    }
                }
            }
        }
    }

    public void SpawnMaze(int fieldWidth, int fieldHeight)
    {
        m_maze = new GameObject
        {
            name = "Maze",
            tag = "Maze"
        };

        generator = new MazeGenerator(fieldWidth, fieldHeight);
        maze = generator.GenerateMaze();

        for (int x = 0; x < maze.GetLength(0); x++)
        {
            for (int y = 0; y < maze.GetLength(1); y++)
            {
                Cell cell = Instantiate(cellPrefab, new Vector2(x, y), Quaternion.identity).GetComponent<Cell>();

                cell.WallLeft.SetActive(maze[x, y].WallLeft);
                cell.WallBottom.SetActive(maze[x, y].WallBottom);

                if (x == 0 || x == maze.GetLength(0) - 1)
                {
                    cell.IsLeftWallAtEdge = true;
                }

                if (y == 0 || y == maze.GetLength(1) - 1)
                {
                    cell.IsBottomWallAtEdge = true;
                }


                // Отключаем бокс коллайдеры на крайних ячейках.
                if (x == maze.GetLength(0) - 1 || y == maze.GetLength(1) - 1)
                {
                    cell.Collider.enabled = false;
                }

                

                cell.transform.SetParent(m_maze.transform);

                // Присваиваем ссылки на сами объекты ячеек.
                maze[x, y].Cell = cell;
            }
        }
    }

    private void OnDestroy()
    {
        GameController.Instance.OnPushButtonPutPlayerOnField -= DisableTriggerCollidersOnCells;
    }
}
