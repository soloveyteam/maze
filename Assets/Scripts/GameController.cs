﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void LevelStartHandler();
public delegate void CreatingNewLevelHandler();

public class GameController : MonoBehaviour
{
    private static GameController m_instance;
    private MazeSpawner m_mazeSpawner;
    private ItemSpawner m_itemSpawner;
    private TrackManager m_trackManager;
    private Inventory m_inventory;
    private bool isPlayerInMove;
    private PlayerController player;
    //private GameObject itemsOnField;
    [SerializeField] private GameObject fieldPrefab;
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private GameObject trackManagerPrefab;
    //[SerializeField] private GameObject itemOnFieldPrefab;

    public event CreatingNewLevelHandler OnPushButtonPutPlayerOnField;

    public static GameController Instance
    {
        get
        {
            if (m_instance == null)
            {
                var controller = Instantiate(Resources.Load("Prefabs/GameController")) as GameObject;
                m_instance = controller.GetComponent<GameController>();
            }
            return m_instance;
        }
    }

    public bool IsPlayerInMove
    {
        get { return isPlayerInMove; }
        set 
        { 
            isPlayerInMove = value;
            HUD.Instance.ChangeButtonInteractivity("Up", !IsPlayerInMove);
            HUD.Instance.ChangeButtonInteractivity("Down", !IsPlayerInMove);
            HUD.Instance.ChangeButtonInteractivity("Left", !IsPlayerInMove);
            HUD.Instance.ChangeButtonInteractivity("Right", !IsPlayerInMove);

            if (!isPlayerInMove && m_trackManager != null)
            {
                DrawTraceLine();
            }
        }
    }

    public PlayerController Player
    {
        get { return player; }
        set { player = value; }
    }

    private void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (m_instance != this) Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        m_mazeSpawner = Instantiate(Resources.Load<MazeSpawner>("Prefabs/MazeSpawner"));
        m_itemSpawner = Instantiate(Resources.Load<ItemSpawner>("Prefabs/ItemSpawner"));

        // Создаём инвентарь.
        m_inventory = new GameObject{name = "Inventory"}.AddComponent<Inventory>();
    }

    public void MovePlayer(Direction direction)
    {
        if (player != null)
        {
            player.SetDirectionOfMovement(direction);
        }
        
    }

    public void CreateNewRandomLevel()
    {
        DestroyCurrentLevel();

        int width = Random.Range(5, 16);
        int height = Random.Range(5, 16);

        m_mazeSpawner.SpawnMaze(width, height);

        Instantiate(fieldPrefab, new Vector2((float)width / 2, (float)height / 2), Quaternion.identity);

        //itemsOnField = new GameObject
        //{
        //    name = "ItemsOnField",
        //    tag = "ItemsOnField"
        //};

        //itemSpawner.SpawnItemsOnField(mazeSpawner.Maze.GetLength(0), mazeSpawner.Maze.GetLength(1));

        ActivatePlayerInstallationForm(true);
    }

    public void CreateNewManualLevel(int width, int height)
    {
        DestroyCurrentLevel();

        m_mazeSpawner.SpawnMaze(width, height);

        Instantiate(fieldPrefab, new Vector2((float)width / 2, (float)height / 2), Quaternion.identity);

        //itemSpawner.SpawnItemsOnField(mazeSpawner.Maze.GetLength(0), mazeSpawner.Maze.GetLength(1));

        ActivatePlayerInstallationForm(true);
    }

    private void DestroyCurrentLevel()
    {
        Destroy(GameObject.FindGameObjectWithTag("Maze"));
        Destroy(GameObject.FindGameObjectWithTag("Field"));
        Destroy(GameObject.FindGameObjectWithTag("Player"));
        Destroy(GameObject.FindGameObjectWithTag("ItemsOnField"));

        if (m_trackManager != null)
        {
            m_trackManager.DestroyCurrentTrackPainters();
        }

        Destroy(GameObject.FindGameObjectWithTag("TrackManager"));
    }

    public void PutPlayerOnField()
    {
        // Получаем координаты выделенной ячейки.
        GameObject selectedCell = GetSelectedCellOnField();

        // Так как игрок двигается по половинчатым координатам типа Vector3(0.5f, 0.5f, 0f),
        // а ячейки имеют точные координаты типа Vector3(0f, 0f, 0f), 
        // то редактируем координаты размещения игрока.
        Vector3 selectedCellPosition = GetSelectedCellOnField().transform.position + new Vector3(0.5f, 0.5f, 0f);
        
        // Создаём игрока в сцене.
        player = Instantiate(playerPrefab, selectedCellPosition, Quaternion.identity).GetComponent<PlayerController>();

        // Телепортируем игрока в выделенную ячейку.
        player.MovePlayerToSelectedCell(selectedCellPosition);

        // Создаём менеджер пути
        m_trackManager = Instantiate(trackManagerPrefab).GetComponent<TrackManager>();
        m_trackManager.AddTrackPainter(selectedCellPosition);

        selectedCell.GetComponent<Cell>().IsCellSelected = false;

        m_itemSpawner.SpawnItemsOnField(m_mazeSpawner.Maze.GetLength(0), m_mazeSpawner.Maze.GetLength(1), player.transform.position);

        OnPushButtonPutPlayerOnField?.Invoke();
    }

    private void ActivatePlayerInstallationForm(bool state)
    {
        HUD.Instance.ActivatePlayerInstallationForm(state);
    }

    public void CellWasClicked(GameObject cellGameObject)
    {
        Cell cell = cellGameObject.GetComponent<Cell>();
        if (cell != null)
        {
            GameObject alreadySelectedCell = GetSelectedCellOnField();

            if (alreadySelectedCell != null && alreadySelectedCell != cellGameObject)
            {
                alreadySelectedCell.GetComponent<Cell>().IsCellSelected = false;
                cell.IsCellSelected = !cell.IsCellSelected;
            }
            else
            {
                cell.IsCellSelected = !cell.IsCellSelected;
            }
        }
    }

    private GameObject GetSelectedCellOnField()
    {
        GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
        for (int i = 0; i < cells.Length; i++)
        {
            Cell cell = cells[i].GetComponent<Cell>();
            if (cell != null && cell.IsCellSelected)
            {
                return cells[i];
            }
        }
        return null;
    }

    private void DrawTraceLine()
    {
        m_trackManager.DrawLineTo(player.transform.position);
    }
}
