﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D m_rigidbody;
    private Vector3 m_direction = new Vector3(0f, 0f, 0f);
    private float speed = 3f;
    private Vector3 m_startCoordinate;
    private Vector3 m_finishCoordinate;
    private Transform m_transform;
    private bool isPlayerInMove = false;

    private bool IsPlayerInMove
    {
        get { return isPlayerInMove; }
        set 
        { 
            isPlayerInMove = value;
            GameController.Instance.IsPlayerInMove = isPlayerInMove;
        }
    }

    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_transform = transform;
        m_finishCoordinate = m_transform.position;
        GameController.Instance.Player = this;
    }

    void Update()
    {
        //if (m_transform.position != m_finishCoordinate)
        //    GameController.Instance.PlayerInMotion();
        transform.position = Vector3.MoveTowards(m_transform.position, m_finishCoordinate, speed * Time.deltaTime);

        if (m_transform.position != m_finishCoordinate)
            IsPlayerInMove = true;
        else
            IsPlayerInMove = false;
    }

    public void SetDirectionOfMovement(Direction direction)
    {
        switch (direction)
        {
            case (Direction.Up):
                m_startCoordinate = transform.position;
                m_direction = Vector3.up;
                m_finishCoordinate = m_startCoordinate + m_direction;
                break;
            case (Direction.Down):
                m_startCoordinate = transform.position;
                m_direction = Vector3.up * -1f;
                m_finishCoordinate = m_startCoordinate + m_direction;
                break;
            case (Direction.Left):
                m_startCoordinate = transform.position;
                m_direction = Vector3.right * -1f;
                m_finishCoordinate = m_startCoordinate + m_direction;
                break;
            case (Direction.Right):
                m_startCoordinate = transform.position;
                m_direction = Vector3.right;
                m_finishCoordinate = m_startCoordinate + m_direction;
                break;
            default:
                Debug.LogError("Неизвестное направление движения.");
                break;
        }
    }

    public void MovePlayerToSelectedCell(Vector3 position)
    {
        //m_finishCoordinate = new Vector3(0.5f, 0.5f, 0f);
        m_finishCoordinate = position;
        //m_transform.position = m_finishCoordinate;
        transform.position = m_finishCoordinate;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Столкновение со стеной.
        if (collision.transform.CompareTag("Wall"))
        {
            m_finishCoordinate = m_startCoordinate;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Вошли в ништяк или выход.
    }
}

public enum Direction { Up, Down, Left, Right}
