﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackManager : MonoBehaviour
{
    [SerializeField] GameObject trackPainterPrefab;
    private List<TrackPainter> trackPainters;

    public void AddTrackPainter(Vector3 startCoordinate)
    {
        if (trackPainters == null)
        {
            trackPainters = new List<TrackPainter>();

            if (trackPainterPrefab != null)
            {
                trackPainters.Add(Instantiate(trackPainterPrefab).GetComponent<TrackPainter>());
                trackPainters[trackPainters.Count - 1].startCoordinate = startCoordinate;
            }
            else
            {
                Debug.LogError("trackPainterPrefab == null");
            }
        }    
        else
        {
            if (trackPainterPrefab != null)
            {
                trackPainters.Add(Instantiate(trackPainterPrefab).GetComponent<TrackPainter>());
                trackPainters[trackPainters.Count - 1].startCoordinate = startCoordinate;
            }
            else
            {
                Debug.LogError("trackPainterPrefab == null");
            }
        }
    }

    public void DrawLineTo(Vector3 to)
    {
        if (trackPainters != null)
        {
            trackPainters[trackPainters.Count - 1].DrawLineTo(to);
        }
    }

    public void DestroyCurrentTrackPainters()
    {
        foreach (var painter in trackPainters)
        {
            Destroy(painter.gameObject);
        }
        trackPainters.Clear();
    }
}
