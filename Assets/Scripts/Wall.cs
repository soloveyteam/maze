﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    [SerializeField] private bool isWallLeft;
    private LineRenderer m_lineRenderer;
    private Cell m_cell;
    private Color oldStartColor;
    private Color oldEndColor;
    private bool isWallVisible = true;

    void Start()
    {
        m_lineRenderer = GetComponent<LineRenderer>();
        m_cell = GetComponentInParent<Cell>();

        // Сохраняем старые цвета.
        oldStartColor = m_lineRenderer.startColor;
        oldEndColor = m_lineRenderer.endColor;

        if (isWallLeft && m_cell.IsLeftWallAtEdge)
        {

        }
        else if (!isWallLeft && m_cell.IsBottomWallAtEdge)
        {

        }
        else
        {
            HideWall();
        }
    }

    private void HideWall()
    {
        Color tempColor = new Color(255, 255, 255);
        tempColor.a = 0f;
        m_lineRenderer.startColor = tempColor;
        m_lineRenderer.endColor = tempColor;

        isWallVisible = false;
    }

    private void ShowWall()
    {
        m_lineRenderer.startColor = oldStartColor;
        m_lineRenderer.endColor = oldEndColor;

        isWallVisible = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            ShowWall();
        }
    }
}
