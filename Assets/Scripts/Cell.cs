﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Cell : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private GameObject wallLeft;
    [SerializeField] private GameObject wallBottom;
    [SerializeField] private GameObject spriteSelected;
    [SerializeField] private BoxCollider2D m_collider;
    private bool isCellSelected = false;
    private bool isLeftWallAtEdge = false;
    private bool isBottomWallAtEdge = false;

    public GameObject WallLeft
    {
        get { return wallLeft; }
        set { wallLeft = value; }
    }

    public GameObject WallBottom
    {
        get { return wallBottom; }
        set { wallBottom = value; }
    }

    public bool IsLeftWallAtEdge
    {
        get { return isLeftWallAtEdge; }
        set { isLeftWallAtEdge = value; }
    }

    public bool IsBottomWallAtEdge
    {
        get { return isBottomWallAtEdge; }
        set { isBottomWallAtEdge = value; }
    }

    public bool IsCellSelected
    {
        get { return isCellSelected; }
        set
        {
            if (isCellSelected != value)
            {
                isCellSelected = value;
                spriteSelected.SetActive(value);
            }
        }
    }

    public BoxCollider2D Collider
    {
        get { return m_collider; }
        set { m_collider = value; }
    }

    private void Start()
    {
        spriteSelected.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        GameController.Instance.CellWasClicked(gameObject);
    }
}
