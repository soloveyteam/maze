﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    private List<InventoryItem> inventoryItems;

    void Start()
    {
        inventoryItems = new List<InventoryItem>();
    }

    private void AddItem(InventoryItem item)
    {
        inventoryItems.Add(item);
    }

    private void DeleteItem(InventoryItem item)
    {

    }

    private void UseItem(InventoryItem item)
    {

    }
}
