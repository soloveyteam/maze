﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemOnField : MonoBehaviour
{
    [SerializeField] private List<Sprite> sprites;
    private ItemType itemType;
    private bool isItemFound;

    private bool IsItemFound
    {
        set 
        {
            isItemFound = value;
            if (isItemFound)
            {
                GetComponentInChildren<SpriteRenderer>().enabled = true;
            }
            else
            {
                GetComponentInChildren<SpriteRenderer>().enabled = false;
            }
        }
    }

    public ItemType ItemType
    {
        get { return itemType; }
        set { itemType = value; }
    }

    void Start()
    {
        if (itemType == ItemType.Key)
        {
            GetComponentInChildren<SpriteRenderer>().sprite = sprites[0];
            gameObject.name = "Key";
            gameObject.tag = "Key";
        }
        else if (itemType == ItemType.Exit)
        {
            GetComponentInChildren<SpriteRenderer>().sprite = sprites[1];
            gameObject.name = "Exit";
            gameObject.tag = "Exit";
        }

        //IsItemFound = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            IsItemFound = true;
        }
    }
}

/*
 * Rope - Верёвка - позволяет перелезть через стену
 * Trap - Капкан - отнимает ходы или жизни
 * Exit - Выход - завершение уровня (срабатывает только при наличии определённого количества ключей в инвентаре)
 * Key - Ключ - необходим, чтобы пройти через выход
 * HealingPotion - восстановление количества жизней или ходов
 * Map - Карта - показывает местоположение всех ключей на уровне
 * Grenade - Граната - позволяет сломать стену
 */

public enum ItemType { Key, Exit, Rope, Trap, HealingPotion, Map, Grenade, Undefined }


