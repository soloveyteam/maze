﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : MonoBehaviour
{
    //private GameObject quad;
    [SerializeField] private Material fieldMaterial;

    private void Start()
    {
        //quad = transform.Find("Quad").gameObject;
        transform.localScale = new Vector3(transform.position.x * 2, transform.position.y * 2, 1);
        fieldMaterial.mainTextureScale = new Vector2(transform.position.x * 2, transform.position.y * 2);

        SetCameraPosition();

        SetCameraSize();
    }

    private void SetCameraPosition()
    {
        // Положение камеры.
        Vector3 cameraPosition = transform.position;
        cameraPosition.z = -10;
        cameraPosition.y -= 2;
        Camera.main.transform.position = cameraPosition;
    }

    private void SetCameraSize()
    {
        // Размер камеры.
        if (transform.localScale.x > transform.localScale.y)
            Camera.main.orthographicSize = transform.localScale.x;
        else
            Camera.main.orthographicSize = transform.localScale.y;
    }
}
