﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackPainter : MonoBehaviour
{
    private LineRenderer m_lineRenderer;

    [HideInInspector]
    public Vector3 startCoordinate;


    void Start()
    {
        m_lineRenderer = GetComponent<LineRenderer>();

        // Создаём нулевую точку Лайн Рендерера в позиции координат игрока.
        m_lineRenderer.positionCount = 1;
        m_lineRenderer.SetPosition(m_lineRenderer.positionCount - 1, startCoordinate);
    }

    public void DrawLineTo(Vector3 to)
    {
        if (m_lineRenderer.GetPosition(m_lineRenderer.positionCount - 1) != to)
        {
            m_lineRenderer.positionCount++;
            m_lineRenderer.SetPosition(m_lineRenderer.positionCount - 1, to);
        }
    }
}
