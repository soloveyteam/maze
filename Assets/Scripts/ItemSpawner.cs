﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField] GameObject m_itemOnFieldPrefab;
    private GameObject m_itemsOnField;

    private ItemSlot[,] itemSlots;
    private Vector3 m_exitCoordinates;
    private Vector3[] m_keysCoordinates;

    public void SpawnItemsOnField(int fieldwidth, int fieldHeight, Vector3 playerCoordinates)
    {
        fieldwidth--;
        fieldHeight--;

        itemSlots = new ItemSlot[fieldwidth, fieldHeight];

        for (int x = 0; x < fieldwidth; x++)
        {
            for (int y = 0; y < fieldHeight; y++)
            {
                itemSlots[x, y] = new ItemSlot(
                    new Vector3((float)x + 0.5f, (float)y + 0.5f, 0.0f),
                    false,
                    ItemType.Undefined);
            }
        }

        m_itemsOnField = new GameObject
        {
            name = "ItemsOnField",
            tag = "ItemsOnField"
        };

        // Количество предметов == 20% от количества ячеек поля.
        int itemsAmount = ((fieldwidth * fieldHeight) / 5);

        for (int i = 0; i < itemsAmount; i++)
        {
            if (i == 0)
            {
                SpawnExit(itemSlots.GetLength(0), itemSlots.GetLength(1), playerCoordinates);
            }
            else if (i == 1)
            {
                //SpawnKey();
            }
            else
            {
                //int x = Random.Range(0, width - 1);
                //int y = Random.Range(0, height - 1);
                Vector3 coordinates = new Vector3(Random.Range(0, fieldwidth - 1), Random.Range(0, fieldHeight - 1), 0.0f);

                
            }
        }
    }

    private void SpawnExit(int fieldwidth, int fieldHeight, Vector3 playerCoordinates)
    {
        Vector3 exitCoordinates = new Vector3(0.5f, 0.5f, 0.0f);

        // Создаём массив с координатами поля.
        Vector3[,] cellsCoordinates = new Vector3[fieldwidth, fieldHeight];
        for (int x = 0; x < cellsCoordinates.GetLength(0); x++)
        {
            for (int y = 0; y < cellsCoordinates.GetLength(1); y++)
            {
                cellsCoordinates[x, y] = new Vector3((float)x + 0.5f, (float)y + 0.5f, 0.0f);

                // Попутно находим самую дальнюю точку от игрока.
                if ((playerCoordinates - cellsCoordinates[x, y]).magnitude > (playerCoordinates - exitCoordinates).magnitude)
                {
                    exitCoordinates = cellsCoordinates[x, y];
                }
            }
        }

        // Тут создаём итем выхода.

        CreateItemOnField(exitCoordinates, ItemType.Exit);

        SpawnKey(exitCoordinates, playerCoordinates, fieldwidth, fieldHeight);
    }

    private void SpawnKey(Vector3 exitCoordinates, Vector3 playerCoordinates, int width, int height)
    {
        Vector3 keyCoordinates = new Vector3(0.5f, 0.5f, 0.0f);

        // Создаём массив с координатами поля.
        Vector3[,] cellsCoordinates = new Vector3[width, height];
        for (int x = 0; x < cellsCoordinates.GetLength(0); x++)
        {
            for (int y = 0; y < cellsCoordinates.GetLength(1); y++)
            {
                cellsCoordinates[x, y] = new Vector3((float)x + 0.5f, (float)y + 0.5f, 0.0f);

                // Попутно находим самую дальнюю точку от игрока.
                if (((playerCoordinates - cellsCoordinates[x, y]).magnitude + (exitCoordinates - cellsCoordinates[x,y]).magnitude) >
                    ((playerCoordinates - keyCoordinates).magnitude + (exitCoordinates - keyCoordinates).magnitude))
                {
                    keyCoordinates = cellsCoordinates[x, y];
                }
            }
        }
        // Тут создаём итем ключ.

        CreateItemOnField(keyCoordinates, ItemType.Key);

    }

    private void CreateItemOnField(Vector3 coordiantes, ItemType itemType)
    {
        GameObject item = Instantiate(m_itemOnFieldPrefab, coordiantes, Quaternion.identity);
        item.GetComponent<ItemOnField>().ItemType = itemType;

        if (m_itemsOnField != null)
        {
            item.transform.SetParent(m_itemsOnField.transform);
        }
        else
        {
            Debug.LogError("itemsOnField == null");
        }
    }

    private class ItemSlot
    {
        public Vector3 coordinate;
        public bool isSlotContainsItem;
        public ItemType itemType;

        public ItemSlot()
        {
            this.coordinate = new Vector3(0.5f, 0.5f, 0.0f);
            this.isSlotContainsItem = false;
            this.itemType = ItemType.Grenade;
        }

        public ItemSlot(Vector3 coordinate, bool isSlotContainsItem, ItemType itemType)
        {
            this.coordinate = coordinate;
            this.isSlotContainsItem = isSlotContainsItem;
            this.itemType = itemType;
        }
    }
}


