﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeGeneratorCell
{
    private int x;
    private int y;

    private bool wallLeft = true;
    private bool wallBottom = true;

    private bool visited = false;

    private int distanceFromStart;

    

    private Cell cell;

    public int X
    {
        get { return x; }
        set { x = value; }
    }

    public int Y
    {
        get { return y; }
        set { y = value; }
    }

    public bool WallLeft
    {
        get { return wallLeft; }
        set { wallLeft = value; }
    }

    public bool WallBottom
    {
        get { return wallBottom; }
        set { wallBottom = value; }
    }

    public bool Visited
    {
        get { return visited; }
        set { visited = value; }
    }

    public int DistanceFromStart
    {
        get { return distanceFromStart; }
        set { distanceFromStart = value; }
    }

    

    public Cell Cell
    {
        get { return cell; }
        set { cell = value; }
    }
}

public class MazeGenerator
{
    private int width;
    private int height;

    public MazeGenerator() 
    { 
        width = 11; 
        height = 11; 
    }

    public MazeGenerator(int width, int height)
    {
        if (width < 5)
        {
            this.width = 6;
        }
        else
        {
            this.width = ++width;
        }

        if (height < 5)
        {
            this.height = 6;
        }
        else
        {
            this.height = ++height;
        }
    }



    public MazeGeneratorCell[,] GenerateMaze()
    {
        MazeGeneratorCell[,] maze = new MazeGeneratorCell[width, height];

        for (int x = 0; x < maze.GetLength(0); x++)
        {
            for (int y = 0; y < maze.GetLength(1); y++)
            {
                maze[x, y] = new MazeGeneratorCell { X = x, Y = y };
            }
        }

        // Отключаем стены, которые торчат сверху лабиринта.
        for (int x = 0; x < maze.GetLength(0); x++)
        {
            maze[x, height - 1].WallLeft = false;
        }

        // Отключаем стены, которые торчат справа от лабиринта.
        for (int y = 0; y < maze.GetLength(1); y++)
        {
            maze[width - 1, y].WallBottom = false;
        }

        RemoveWallsWithBacktracker(maze);

        //PlaceMazeExit(maze);

        return maze;
    }

    private void RemoveWallsWithBacktracker(MazeGeneratorCell[,] maze)
    {
        MazeGeneratorCell current = maze[0, 0];
        current.Visited = true;
        current.DistanceFromStart = 0;

        Stack<MazeGeneratorCell> stack = new Stack<MazeGeneratorCell>();
        do
        {
            List<MazeGeneratorCell> unvisitedneighbours = new List<MazeGeneratorCell>();

            int x = current.X;
            int y = current.Y;

            if ((x > 0) && (!maze[x - 1, y].Visited))
                unvisitedneighbours.Add(maze[x - 1, y]);
            if ((y > 0) && (!maze[x, y - 1].Visited))
                unvisitedneighbours.Add(maze[x, y - 1]);
            if ((x < width - 2) && (!maze[x + 1, y].Visited))
                unvisitedneighbours.Add(maze[x + 1, y]);
            if ((y < height - 2) && (!maze[x, y + 1].Visited))
                unvisitedneighbours.Add(maze[x, y + 1]);

            if (unvisitedneighbours.Count > 0)
            {
                MazeGeneratorCell chosen = unvisitedneighbours[Random.Range(0, unvisitedneighbours.Count)];
                RemoveWall(current, chosen);

                chosen.Visited = true;
                stack.Push(chosen);
                current = chosen;
                chosen.DistanceFromStart = stack.Count;
            }
            else
            {
                current = stack.Pop();
            }

        } while (stack.Count > 0);
    }

    private void RemoveWall(MazeGeneratorCell current, MazeGeneratorCell chosen)
    {
        if (current.X == chosen.X)
        {
            if (current.Y > chosen.Y)
                current.WallBottom = false;
            else
                chosen.WallBottom = false;
        }
        else
        {
            if (current.X > chosen.X)
                current.WallLeft = false;
            else
                chosen.WallLeft = false;
        }
    }

    private void PlaceMazeExit(MazeGeneratorCell[, ] maze)
    {
        // Проверяем все ячейки на то, которая самая дальняя от старта.
        MazeGeneratorCell furthest = maze[0, 0];

        // Проходимся по верхнему и нижнему краю.
        for (int x = 0; x < maze.GetLength(0); x++)
        {
            if (maze[x, height - 2].DistanceFromStart > furthest.DistanceFromStart)
                furthest = maze[x, height - 2];
            if (maze[x, 0].DistanceFromStart > furthest.DistanceFromStart)
                furthest = maze[x, 0];
        }

        // Проходимся по правому и лувому краю.
        for (int y = 0; y < maze.GetLength(1); y++)
        {
            if (maze[width - 2, y].DistanceFromStart > furthest.DistanceFromStart)
                furthest = maze[width - 2, y];
            if (maze[0, y].DistanceFromStart > furthest.DistanceFromStart)
                furthest = maze[0, y];
        }

        // Нашли самую дальнюю ячейку и выключаем её стену вне лабиринта.
        if (furthest.X == 0)
            furthest.WallLeft = false;
        else if (furthest.Y == 0)
            furthest.WallBottom = false;
        else if (furthest.X == width - 2)
            maze[furthest.X + 1, furthest.Y].WallLeft = false;
        else if (furthest.Y == height - 2)
            maze[furthest.X, furthest.Y + 1].WallBottom = false;
    }
}
