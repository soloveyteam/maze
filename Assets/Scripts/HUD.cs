﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    private static HUD m_instance;

    [SerializeField] private Button buttonUp;
    [SerializeField] private Button buttonDown;
    [SerializeField] private Button buttonLeft;
    [SerializeField] private Button buttonRight;
    [SerializeField] private Button buttonPutPlayerOnField;
    [SerializeField] private GameObject playerInstallationForm;
    [SerializeField] private Dropdown dropdownWidth;
    [SerializeField] private Dropdown dropdownHeight;

    public static HUD Instance
    {
        get
        {
            return m_instance;
        }
    }

    private void Awake()
    {
        m_instance = this;
    }

    private void Start()
    {
        ActivatePlayerInstallationForm(false);
        //GameController.Instance.LevelStarted += HandleOnLevelStarted;
    }

    public void ButtonUp()
    {
        GameController.Instance.MovePlayer(Direction.Up);
    }

    public void ButtonDown()
    {
        GameController.Instance.MovePlayer(Direction.Down);
    }

    public void ButtonLeft()
    {
        GameController.Instance.MovePlayer(Direction.Left);
    }

    public void ButtonRight()
    {
        GameController.Instance.MovePlayer(Direction.Right);
    }

    public void ButtonCreateNewRandomLevel()
    {
        GameController.Instance.CreateNewRandomLevel();
    }

    public void ButtonCreateNewManualLevel()
    {
        GameController.Instance.CreateNewManualLevel(dropdownWidth.value + 5, dropdownHeight.value + 5);
    }

    public void ButtonPutPlayerOnField()
    {
        GameController.Instance.PutPlayerOnField();
        ActivatePlayerInstallationForm(false);
    }

    public void ChangeButtonInteractivity(string buttonName, bool interactivity)
    {
        switch (buttonName)
        {
            case "Up":
                buttonUp.interactable = interactivity;
                break;
            case "Down":
                buttonDown.interactable = interactivity;
                break;
            case "Left":
                buttonLeft.interactable = interactivity;
                break;
            case "Right":
                buttonRight.interactable = interactivity;
                break;
            default:
                Debug.LogError("Wrong buttonName");
                break;
        }
    }

    public void ShowWindow(CanvasGroup window)
    {
        window.alpha = 1f;
        window.blocksRaycasts = true;
        window.interactable = true;
    }

    public void HideWindow(CanvasGroup window)
    {
        window.alpha = 0f;
        window.blocksRaycasts = false;
        window.interactable = false;
    }

    public void ActivatePlayerInstallationForm(bool state)
    {
        playerInstallationForm.SetActive(state);
    }

    private void HandleOnLevelStarted()
    {

    }

    private void OnDestroy()
    {
        //GameController.Instance.LevelStarted -= HandleOnLevelStarted;
    }
}
